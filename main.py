from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/')
def home():
		data = "hello world"
		return jsonify({'data': data})

@app.route('/test')
def test():
		return "test2"

@app.route('/spam')
def egg():
		return("spam & eggs!")

if __name__ == '__main__':
		app.run(host='0.0.0.0', port=5000)
